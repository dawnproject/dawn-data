models/relics/relic
{
	dpreflectcube cubemaps/default/sky
 	{
		map models/relics/relic.tga
		rgbgen lightingDiffuse
	}
}
models/relics/relic_ring
{
 	{
		map models/relics/relic_ring.tga
		blendfunc GL_SRC_ALPHA GL_ONE
		rgbgen lightingDiffuse
	}
}
models/relics/sign_resistance
{
	cull none
 	{
		map models/relics/sign_resistance.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_regeneration
{
	cull none
 	{
		map models/relics/sign_regeneration.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_vampire
{
	cull none
 	{
		map models/relics/sign_vampire.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_ammo
{
	cull none
 	{
		map models/relics/sign_ammo.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_damage
{
	cull none
 	{
		map models/relics/sign_damage.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_splashdamage
{
	cull none
 	{
		map models/relics/sign_splashdamage.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_firingspeed
{
	cull none
 	{
		map models/relics/sign_firingspeed.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_disability
{
	cull none
 	{
		map models/relics/sign_disability.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_teamboost
{
	cull none
 	{
		map models/relics/sign_teamboost.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_speed
{
	cull none
 	{
		map models/relics/sign_speed.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_jump
{
	cull none
 	{
		map models/relics/sign_jump.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_flight
{
	cull none
 	{
		map models/relics/sign_flight.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_invisible
{
	cull none
 	{
		map models/relics/sign_invisible.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_radioactive
{
	cull none
 	{
		map models/relics/sign_radioactive.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_resurrection
{
	cull none
 	{
		map models/relics/sign_resurrection.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
models/relics/sign_vengeance
{
	cull none
 	{
		map models/relics/sign_vengeance.tga
		blendfunc add
		rgbgen lightingDiffuse
	}
}
